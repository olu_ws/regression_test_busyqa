*** Settings ***
Library    SeleniumLibrary
Resource   ../Utilities/parser_utilities/test_parser.robot      
Test Teardown    Close All Browsers   
Suite Setup    Log To Console    Suite Execution Started ... 
Suite Teardown    Log To Console    Suite Execution Completed ... 

*** Variables ***
${live_postings_button}     //a[@href = "/newsearchserviceneed"]     

*** Test Cases ***

TP_002
    ${base_url}=    Get Base Url
    Open Browser    ${base_url}    gc
    Click Element    //a[@href = "/signup"]
    Sleep    5s    
    
         
TP_003
    ${base_url}=    Get Base Url
    Open Browser    ${base_url}    gc
    Click Element    ${live_postings_button}
    Sleep    5s   
      
   
    
    
    
    
TP_004
    ${base_url}=    Get Base Url
    Open Browser    ${base_url}    gc
    Click Element    ${live_postings_button}
    Click Element    id= tabs        
    
    
    
TP_009
    ${base_url}=    Get Base Url
    Open Browser    ${base_url}    gc
    Click Element    ${live_postings_button}
    Click Element    id= tabs    
    Click Element    (//div[@class = "jq-selectbox__trigger"])[3]    
    Sleep    5s  
    
    
    
    
TP_010
    ${base_url}=    Get Base Url
    Open Browser    ${base_url}    gc
    Click Element    //a[@href = "/contactus"]
    Input Text    name=email    olumidews@yahoo.com  
    Input Text    id=contactSubject    Complaint  
    Input Text    id=contactContent    The service you provided was not satisfactory, I would like a refund
    Sleep    5s  
    
    
    