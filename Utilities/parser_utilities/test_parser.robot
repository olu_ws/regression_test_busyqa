*** Settings ***

Library    OperatingSystem
Library    String
Library    RequestsLibrary
Library    Collections
Resource    ../misc_utilities/misc_utilties.robot

*** Keywords ***
# Get Admin Credentials
    # ${test_bed_file}=    Get Test Bed File
    # ${testBed_json}=    To Json    ${test_bed_file} # Line TO convert
    # ${credentials}=    Get From Dictionary    ${testBed_json}    credentials
    # ${username}=    Get From Dictionary    ${credentials}    username
    # ${password}=    Get From Dictionary    ${credentials}    password
    # [Return]    ${username}    ${password}
    
Get Base Url
    ${test_bed_file}=    Get Test Bed File
    ${testBed_json}=    To Json    ${test_bed_file}
    ${base_url}=    Get From Dictionary    ${testBed_json}    base_url
    [Return]    ${base_url}
    
Get Test Bed File
    ${home_dir}=    Get Home Dir
    ${test_bed_file}=    Get File    ${home_dir}//config//browser_info//test_frame.json
    [Return]    ${test_bed_file}